/*!
  _   _  ___  ____  ___ ________  _   _   _   _ ___   ____  ____   ___  
 | | | |/ _ \|  _ \|_ _|__  / _ \| \ | | | | | |_ _| |  _ \|  _ \ / _ \ 
 | |_| | | | | |_) || |  / / | | |  \| | | | | || |  | |_) | |_) | | | |
 |  _  | |_| |  _ < | | / /| |_| | |\  | | |_| || |  |  __/|  _ <| |_| |
 |_| |_|\___/|_| \_\___/____\___/|_| \_|  \___/|___| |_|   |_| \_\\___/ 
                                                                                                                                                                                                                                                                                                                                       
=========================================================
* Horizon UI Dashboard PRO - v1.0.0
=========================================================

* Product Page: https://www.horizon-ui.com/pro/
* Copyright 2022 Horizon UI (https://www.horizon-ui.com/)

* Designed and Coded by Simmmple

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React, { useState, useEffect } from "react";
import { useAuthContext } from "../../../../hooks/useAuthContext";

// Chakra imports
import { Box, Flex, useColorModeValue, Button, Text } from "@chakra-ui/react";

// Custom components
import Template from "layouts/producer/Default";
import Products from "../products";

export default function Event() {
  let mainText = useColorModeValue("navy.700", "white");
  const { user } = useAuthContext();
  const [event, setEvent] = useState();
  const [eventPath, setEventPath] = useState("");
  const [inProgress, setInProgress] = useState(false);

  useEffect(() => {
    const fetchEvent = async () => {
      if (inProgress) return;
      if (!user) return;
      setInProgress(true);
      let hash = window.location.hash;
      let path = process.env.REACT_APP_API_URL + hash.replace("#", "");
      setEventPath(path);
      const response = await fetch(path, {
        method: "GET",
        headers: {
          "Access-Control-Allow-Credentials": true,
          "Content-Type": "application/json",
          Authorization: user,
        },
      });
      const json = await response.json();
      setInProgress(false);
      if (response.ok) {
        setEvent(json.event);
        console.log("GET EVENT RES: ", json.event);
      }
    };

    if (user) {
      fetchEvent();
    }
  }, [user]);

  // Chakra Color Mode
  return (
    <Template>
      {event && (
        <Flex
          w="100%"
          textAlign={"center"}
          justifyContent={"center"}
          alignItems={"center"}
          flexDirection="column"
        >
          <Flex
            w="90%"
            minW="160px"
            marginBottom={"10px"}
            justifyContent={"center"}
          >
            <Text
              color={mainText}
              fontWeight="bold"
              fontSize={{ base: "24px", md: "34px" }}
              mb="30px"
            >
              {event.name}
            </Text>
            <Button
              ml="20px"
              mt="5px"
              variant={"action"}
              fontSize="sm"
              fontWeight="500"
              borderRadius="70px"
              px="24px"
              py="5px"
            >
              {event.status}
            </Button>
          </Flex>
          <Products event_path={eventPath} user={user} />
        </Flex>
      )}
    </Template>
  );
}
