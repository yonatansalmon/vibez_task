import { Button, Flex, Table, Tbody, Text, Th, Thead, Tr, useColorModeValue } from "@chakra-ui/react";
import React, { useMemo, useEffect, useState, useCallback } from "react";
import { useGlobalFilter, usePagination, useSortBy, useTable } from "react-table";

// Custom components
import Card from "components/card/Card";
// Assets
import { product_type_dict, currency_dict } from "dictionaries";
import { ProductPopup } from "./ProductPopup";
import { Product } from "./Product";

export function ProductsTable(props) {
  const { user, fetchProducts, columnsData, tableData, path, editProduct } = props;

  const columns = useMemo(() => columnsData, [columnsData]);
  const data = useMemo(() => tableData, [tableData]);

  const tableInstance = useTable({ columns, data }, useGlobalFilter, useSortBy, usePagination);

  const { getTableProps, getTableBodyProps, headerGroups, initialState } = tableInstance;
  initialState.pageSize = 10;

  const borderColor = useColorModeValue("gray.200", "whiteAlpha.100");
  const [showDetails, setShowDetails] = useState(false);
  const [product, setProduct] = useState();

  const toggleClosure = useCallback(
    (item) => {
      setProduct(item);
      setShowDetails((prevShowDetails) => !prevShowDetails);
    },
    [setProduct, setShowDetails]
  );

  const renderProductPopup = () => {
    if (showDetails)
      return (
        <ProductPopup
          path={path}
          user={user}
          product={product}
          toggleClosure={toggleClosure}
          fetchProducts={fetchProducts}
          editProduct={editProduct}
        />
      );
  };

  const renderBody = useMemo(() => {
    return tableData && tableData.map((item) => <Product toggleClosure={toggleClosure} item={item} key={item.id} />);
  }, [tableData, toggleClosure]);

  return (
    <Card direction="column" w="100%" px="0px" overflowX={{ sm: "scroll", lg: "hidden" }}>
      {renderProductPopup()}
      <Table {...getTableProps()} variant="simple" color="gray.500" mb="24px">
        <Thead>
          {headerGroups.map((headerGroup, index) => (
            <Tr {...headerGroup.getHeaderGroupProps()} key={index}>
              {headerGroup.headers.map((column, index) => (
                <Th {...column.getHeaderProps(column.getSortByToggleProps())} pe="10px" key={index} borderColor={borderColor}>
                  <Flex justify="space-between" align="center" fontSize={{ sm: "10px", lg: "12px" }} color="gray.400">
                    {column.render("Header")}
                  </Flex>
                </Th>
              ))}
            </Tr>
          ))}
        </Thead>
        <Tbody {...getTableBodyProps()}>{renderBody}</Tbody>
      </Table>
    </Card>
  );
}
