import React, { useState, useEffect } from "react";

// Chakra imports
import { Box, Flex, Text } from "@chakra-ui/react";

// Custom components
import { columnsDataProducts } from "./components/ColumnsData";
import { AddButton } from "components/buttons/AddButton";
// Assets
import { ProductsTable } from "./components/ProductsTable";
import FormCard from "./components/FormCard";
import { ProductPopup } from "./components/ProductPopup";
import { Spinner } from "components/spinner";

export default function Products({ event_path, user }) {
  const [products, setProducts] = useState([]);
  const [productPath, setProductPath] = useState("");
  const [error, setError] = useState(null);
  const [showDetails, setShowDetails] = useState(false);
  const [inProgress, setInProgress] = useState(false);

  const fetchProducts = async (path) => {
    if (inProgress) return;
    if (!user) return;
    setInProgress(true);
    const response = await fetch(path, {
      method: "GET",
      headers: {
        "Access-Control-Allow-Credentials": true,
        "Content-Type": "application/json",
        Authorization: user,
      },
    });
    const json = await response.json();
    console.log("GET PRODUCTS RES: ", json);
    setInProgress(false);
    if (response.ok) {
      setProducts(json.products);
    } else {
      setError(response?.error?.message);
    }
  };

  const toggleClosure = () => {
    setShowDetails(!showDetails);
  };

  const renderProductPopup = () => {
    if (showDetails)
      return <ProductPopup path={productPath} user={user} toggleClosure={toggleClosure} fetchProducts={fetchProducts} editProduct={editProduct} />;
  };

  useEffect(() => {
    localStorage.setItem("event_route", "products");
    if (user && event_path) {
      let path = event_path + "/products";
      setProductPath(path);
      fetchProducts(path);
    }
  }, [user]);

  const editProduct = (newProduct) => {
    const newProductList = [...products].map((product) => {
      if (newProduct.id === product.id) {
        return newProduct;
      } else {
        return product;
      }
    });

    setProducts(newProductList);
  };

  // Chakra Color Mode
  return (
    <Flex w="100%" justifyContent={"center"} direction={"column"} alignItems={"center"}>
      <Flex w="100%" marginBottom={"10px"} justifyContent={"center"} paddingEnd="40px">
        <AddButton text={"Create Product"} action={toggleClosure} />
      </Flex>
      {error && <Text color="red">{error}</Text>}
      {/* {showForm > 0 && (
        <FormCard
          user={user}
          path={productPath}
          fetchProducts={fetchProducts}
        />
      )} */}
      {renderProductPopup()}
      {inProgress && <Spinner />}
      {products && products.length > 0 && (
        <ProductsTable
          user={user}
          setError={setError}
          products={products}
          columnsData={columnsDataProducts}
          fetchProducts={fetchProducts}
          tableData={products}
          path={productPath}
          editProduct={editProduct}
        />
      )}
    </Flex>
  );
}
