import React, { useState, useEffect } from "react";

// Chakra imports
import { Flex, Button, Link, Text, useColorModeValue } from "@chakra-ui/react";

// Custom components
import { ItemsTable } from "./ItemsTable";
// import { SearchBar } from "components/navbar/searchBar/SearchBar";

export function ItemsView(props) {
  const [input, setInput] = useState("");
  let mainText = useColorModeValue("navy.700", "white");

  // Chakra Color Mode
  return (
    <Flex
      w="100%"
      textAlign={"center"}
      justifyContent={"center"}
      alignItems={"center"}
      flexDirection="column"
    >
      <Flex w="90%" minW="160px" marginBottom={"30px"} direction="column">
        <Text
          color={mainText}
          fontWeight="bold"
          fontSize="14px"
          mb="10px"
          alignSelf={"flex-start"}
        >
          {" Total items: " + props?.tableData?.length}
        </Text>
        {/* <SearchBar
          placeholder={"Search by order number..."}
          action={setInput}
        /> */}
      </Flex>
      <ItemsTable {...props} />
    </Flex>
  );
}
